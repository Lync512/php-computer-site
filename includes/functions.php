<?php

function get_cpu_inventory($db, $cpu_type) {

    $query = "SELECT * from inventory_cpu where category=:cpu_type";
    $statement = $db->prepare($query);
    $statement->bindValue(':cpu_type', $cpu_type);
    $statement->execute();
    $info = $statement->fetchAll(PDO::FETCH_ASSOC);
    $statement->closeCursor();
    return $info;
}

function get_cpu_html($cpu) {


    $image_file = "../img/build_assets/cpu/" . $cpu['upc'] . ".png";

    $formatted_price = sprintf("$%.2f", $cpu['price']);

    $upc = $cpu['upc'];
    $name = $cpu['name'];
    $html_out = "";


    $html_out = <<<EOD
        <figure>
          <img src="{$image_file}" alt="{$upc}">
          <figcaption>
            <span class='category'>{$name}</span><br>
            {$formatted_price}<br>
            {$upc}
            <form action="../build_pages/cpu.php" method='post'>
              <input type="hidden" name="upc" value="{$upc}">
              <input type="submit" class="build_button" value="Add to Cart">        
            </form>
        </figcaption>
      </figure>
EOD;

    return $html_out;
}

function get_mobo_inventory($db, $mobo_type) {

    $query = "SELECT * from inventory_mobo where category=:mobo_type";
    $statement = $db->prepare($query);
    $statement->bindValue(':mobo_type', $mobo_type);
    $statement->execute();
    $info = $statement->fetchAll(PDO::FETCH_ASSOC);
    $statement->closeCursor();
    return $info;
}

function get_mobo_html($mobo) {


    $image_file = "../img/build_assets/mobo/" . $mobo['upc'] . ".png";

    $formatted_price = sprintf("$%.2f", $mobo['price']);

    $html_out = "";


    $html_out = <<<EOD
        <figure>
          <img src="{$image_file}" alt="{$mobo['upc']}">
          <figcaption>
            <span class='category'>{$mobo['name']}</span><br>
            {$formatted_price}<br>
           
            <form action="../build_pages/mobo.php" method='post'>
              <input type="hidden" name="upc" value={$mobo['upc']}>
              <input type="submit" value="Add to Cart">        
            </form>
        </figcaption>
      </figure>
EOD;

    return $html_out;
}

function get_gfx_inventory($db) {

    $query = "SELECT * from inventory_gfx";
    $statement = $db->prepare($query);
    $statement->execute();
    $info = $statement->fetchAll(PDO::FETCH_ASSOC);
    $statement->closeCursor();
    return $info;
}

function get_gfx_html($gfx) {

//echo $gfx['upc'];
    $image_file = "../img/build_assets/gfx/" . $gfx['upc'] . ".png";

    $formatted_price = sprintf("$%.2f", $gfx['price']);

    $html_out = "";


    $html_out = <<<EOD
        <figure>
          <img src="{$image_file}" alt="{$gfx['upc']}">
          <figcaption>
            <span class='category'>{$gfx['name']}</span><br>
            {$formatted_price}<br>
           
            <form action="../build_pages/gfx.php" method='post'>
              <input type="hidden" name="upc" value={$gfx['upc']}>
              <input type="submit" value="Add to Cart">        
            </form>
        </figcaption>
      </figure>
EOD;

    return $html_out;
}

function get_ram_inventory($db) {

    $query = "SELECT * from inventory_ram";
    $statement = $db->prepare($query);
    $statement->execute();
    $info = $statement->fetchAll(PDO::FETCH_ASSOC);
    $statement->closeCursor();
    return $info;
}

function get_ram_html($ram) {


    $image_file = "../img/build_assets/ram/" . $ram['upc'] . ".png";

    $formatted_price = sprintf("$%.2f", $ram['price']);

    $html_out = "";


    $html_out = <<<EOD
        <figure>
          <img src="{$image_file}" alt="{$ram['upc']}">
          <figcaption>
            <span class='category'>{$ram['name']}</span><br>
            {$formatted_price}<br>
           
            <form action="../build_pages/ram.php" method='post'>
              <input type="hidden" name="upc" value={$ram['upc']}>
              <input type="submit" value="Add to Cart">        
            </form>
        </figcaption>
      </figure>
EOD;

    return $html_out;
}

function get_storage_inventory($db) {

    $query = "SELECT * from inventory_storage";
    $statement = $db->prepare($query);
    $statement->execute();
    $info = $statement->fetchAll(PDO::FETCH_ASSOC);
    $statement->closeCursor();
    return $info;
}

function get_storage_html($sto) {


    $image_file = "../img/build_assets/storage/" . $sto['upc'] . ".png";

    $formatted_price = sprintf("$%.2f", $sto['price']);

    $html_out = "";


    $html_out = <<<EOD
        <figure>
          <img src="{$image_file}" alt="{$sto['upc']}">
          <figcaption>
            <span class='category'>{$sto['name']}</span><br>
            {$formatted_price}<br>
           
            <form action="../build_pages/storage.php" method='post'>
              <input type="hidden" name="upc" value={$sto['upc']}>
              <input type="submit" value="Add to Cart">        
            </form>
        </figcaption>
      </figure>
EOD;

    return $html_out;
}

function get_fan_inventory($db) {

    $query = "SELECT * from inventory_fan";
    $statement = $db->prepare($query);
    $statement->execute();
    $info = $statement->fetchAll(PDO::FETCH_ASSOC);
    $statement->closeCursor();
    return $info;
}

function get_fan_html($fan) {


    $image_file = "../img/build_assets/fan/" . $fan['upc'] . ".png";

    $formatted_price = sprintf("$%.2f", $fan['price']);

    $html_out = "";


    $html_out = <<<EOD
        <figure>
          <img src="{$image_file}" alt="{$fan['upc']}">
          <figcaption>
            <span class='category'>{$fan['name']}</span><br>
            {$formatted_price}<br>
           
            <form action="../build_pages/fan.php" method='post'>
              <input type="hidden" name="upc" value={$fan['upc']}>
              <input type="submit" value="Add to Cart">        
            </form>
        </figcaption>
      </figure>
EOD;

    return $html_out;
}

function get_cooler_inventory($db, $cooler_type) {

    $query = "SELECT * from inventory_cooler where category=:cooler_type";
    $statement = $db->prepare($query);
    $statement->bindValue(':cooler_type', $cooler_type);
    $statement->execute();
    $info = $statement->fetchAll(PDO::FETCH_ASSOC);
    $statement->closeCursor();
    return $info;
}

function get_cooler_html($cool) {


    $image_file = "../img/build_assets/cooler/" . $cool['upc'] . ".png";

    $formatted_price = sprintf("$%.2f", $cool['price']);

    $html_out = "";


    $html_out = <<<EOD
        <figure>
          <img src="{$image_file}" alt="{$cool['upc']}">
          <figcaption>
            <span class='category'>{$cool['name']}</span><br>
            {$formatted_price}<br>
           
            <form action="../build_pages/fan.php" method='post'>
              <input type="hidden" name="upc" value={$cool['upc']}>
              <input type="submit" value="Add to Cart">        
            </form>
        </figcaption>
      </figure>
EOD;

    return $html_out;
}

function get_keyboard_inventory($db) {

    $query = "SELECT * from inventory_keyboard";
    $statement = $db->prepare($query);
    $statement->execute();
    $info = $statement->fetchAll(PDO::FETCH_ASSOC);
    $statement->closeCursor();
    return $info;
}

function get_keyboard_html($keyb) {


    $image_file = "../img/build_assets/keyboard/" . $keyb['upc'] . ".png";

    $formatted_price = sprintf("$%.2f", $keyb['price']);

    $html_out = "";


    $html_out = <<<EOD
        <figure>
          <img src="{$image_file}" alt="{$keyb['upc']}">
          <figcaption>
            <span class='category'>{$keyb['name']}</span><br>
            {$formatted_price}<br>
           
            <form action="../build_pages/keyboard.php" method='post'>
              <input type="hidden" name="upc" value={$keyb['upc']}>
              <input type="submit" value="Add to Cart">        
            </form>
        </figcaption>
      </figure>
EOD;

    return $html_out;
}

function get_mouse_inventory($db) {

    $query = "SELECT * from inventory_mouse";
    $statement = $db->prepare($query);
    $statement->execute();
    $info = $statement->fetchAll(PDO::FETCH_ASSOC);
    $statement->closeCursor();
    return $info;
}

function get_mouse_html($mous) {


    $image_file = "../img/build_assets/mouse/" . $mous['upc'] . ".png";

    $formatted_price = sprintf("$%.2f", $mous['price']);

    $html_out = "";


    $html_out = <<<EOD
        <figure>
          <img src="{$image_file}" alt="{$mous['upc']}">
          <figcaption>
            <span class='category'>{$mous['category']}</span><br>
            {$formatted_price}<br>
           
            <form action="../build_pages/mouse.php" method='post'>
              <input type="hidden" name="upc" value={$mous['upc']}>
              <input type="submit" value="Add to Cart">        
            </form>
        </figcaption>
      </figure>
EOD;

    return $html_out;
}

function get_psu_inventory($db) {

    $query = "SELECT * from inventory_psu";
    $statement = $db->prepare($query);
    $statement->execute();
    $info = $statement->fetchAll(PDO::FETCH_ASSOC);
    $statement->closeCursor();
    return $info;
}

function get_psu_html($psu) {


    $image_file = "../img/build_assets/psu/" . $psu['upc'] . ".png";

    $formatted_price = sprintf("$%.2f", $psu['price']);

    $html_out = "";


    $html_out = <<<EOD
        <figure>
          <img src="{$image_file}" alt="{$psu['upc']}">
          <figcaption>
            <span class='category'>{$psu['category']}</span><br>
            {$formatted_price}<br>
           
            <form action="../build_pages/psu.php" method='post'>
              <input type="hidden" name="upc" value={$psu['upc']}>
              <input type="submit" value="Add to Cart">        
            </form>
        </figcaption>
      </figure>
EOD;

    return $html_out;
}

function get_case_inventory($db) {

    $query = "SELECT * from inventory_case";
    $statement = $db->prepare($query);
    $statement->execute();
    $info = $statement->fetchAll(PDO::FETCH_ASSOC);
    $statement->closeCursor();
    return $info;
}

function get_case_html($case) {


    $image_file = "../img/build_assets/case/" . $case['upc'] . ".png";

    $formatted_price = sprintf("$%.2f", $case['price']);

    $upc = $case['upc'];
    $name = $case['name'];
    $html_out = "";


    $html_out = <<<EOD
        <figure>
          <img src="{$image_file}" alt="{$upc}">
          <figcaption>
            <span class='category'>{$name}</span><br>
            {$formatted_price}<br>
            {$upc}
            <form action="../build_pages/cpu.php" method='post'>
              <input type="hidden" name="upc" value="{$upc}">
              
              <input type="submit" value="Add to Cart">        
            </form>
        </figcaption>
      </figure>
EOD;

    return $html_out;
}

function get_monitor_inventory($db) {

    $query = "SELECT * from inventory_monitor";
    $statement = $db->prepare($query);
    $statement->execute();
    $info = $statement->fetchAll(PDO::FETCH_ASSOC);
    $statement->closeCursor();
    return $info;
}

function get_monitor_html($mon) {


    $image_file = "../img/build_assets/monitor/" . $mon['upc'] . ".png";

    $formatted_price = sprintf("$%.2f", $mon['price']);

    $html_out = "";


    $html_out = <<<EOD
        <figure>
          <img src="{$image_file}" alt="{$mon['upc']}">
          <figcaption>
            <span class='category'>{$mon['name']}</span><br>
            {$formatted_price}<br>
           
            <form action="../build_pages/monitor.php" method='post'>
              <input type="hidden" name="upc" value={$mon['upc']}>
              <input type="submit" value="Add to Cart">        
            </form>
        </figcaption>
      </figure>
EOD;

    return $html_out;
}

function add_cart($db, $upc, $username) {

    $query = 'INSERT INTO cart (upc, quantity, username) values(:upc, 1, :username) ON DUPLICATE KEY UPDATE quantity = quantity+1';
    $statement = $db->prepare($query);
    $statement->bindValue(':upc', $upc);
    $statement->bindValue(':username', $username);
    $statement->execute();
    $statement->closeCursor();
}

function add_category($db, $category_name, $category_description) {
    $sql = 'INSERT INTO forum_categories (cat_name, cat_description) VALUES (:category_name, :category_description)';
    $statement = $db->prepare($sql);
    $statement->bindValue(':category_name', $category_name);
    $statement->bindValue(':category_description', $category_description);
    $statement->execute();
    $statement->closeCursor();
    if (!$statement) {
//dispay error
        echo 'ERROR';
    } else {
        echo 'New category sucessfully added';
    }
}

function get_topics($db) {
    $sql = "SELECT * FROM forum_topics";
    $statement = $db->prepare($sql);
    $statement->execute();
    $result = $statement->fetchAll(PDO::FETCH_ASSOC);
    $statement->closeCursor();
    //print_r($result);

    return $result;
}

function new_topic($db, $topic_name) {
    $sql = "INSERT INTO forum_topics(name) VALUES(:name)";
    $statement = $db->prepare($sql);
    $statement->bindValue(':name', $topic_name);
    $statement->execute();
    $statement->closeCursor();
}

function new_post($db, $post_content, $topic_name, $post_by) {


    $sql = "INSERT INTO posts(post_content,post_topic, post_by) VALUES (:post_content, :topic_name,:post_by)";
    $statement = $db->prepare($sql);
    $statement->bindValue(':post_content', $post_content);
    $statement->bindValue(':topic_name', $topic_name);
    $statement->bindValue(':post_by', $post_by);

    $statement->execute();
    $statement->closeCursor();
}

function get_user_id($db, $username) {

    $query = "SELECT user_id from users WHERE username = :username";
    $statement = $db->prepare($query);
    $statement->bindValue(':username', $username);
    $statement->execute();
    $info = $statement->fetch(PDO::FETCH_ASSOC);
    $statement->closeCursor();
    return $info['user_id'];
}

function get_posts($db) {
    $sql = "SELECT * FROM posts";
    $statement = $db->prepare($sql);
    $statement->execute();
    $result = $statement->fetchAll(PDO::FETCH_ASSOC);
    $statement->closeCursor();
    //print_r($result);

    return $result;
}

function get_cart($db, $username) {
    $query = 'SELECT * from cart where username=:username';
    $statement = $db->prepare($query);
    $statement->bindValue(':username', $username);
    $statement->execute();
    $info = $statement->fetchAll(PDO::FETCH_ASSOC);
    $statement->closeCursor();
    return $info;
}

function clear_cart($db, $username) {

    $query = 'DELETE from cart where username = :username';
    $statement = $db->prepare($query);
    $statement->bindValue(':username', $username);
    $statement->execute();
    $statement->closeCursor();
}

function getQuantityAvailable($db, $upc) {

    $query = "SELECT quantity from inventory WHERE upc = :upc ";
    $statement = $db->prepare($query);
    $statement->bindValue(':upc', $upc);
    $statement->execute();
    $info = $statement->fetch(PDO::FETCH_ASSOC);
    $statement->closeCursor();

    if ($info == null) {
        return 0;
    } else {
        return $info['quantity'];
    }
}

function getQuantityInCart($db, $upc) {

    $query = "SELECT quantity from cart WHERE upc = :upc";
    $statement = $db->prepare($query);
    $statement->bindValue(':upc', $upc);
    $statement->execute();
    $info = $statement->fetch(PDO::FETCH_ASSOC);
    $statement->closeCursor();


    if ($info == null) {
        return 0;
    } else {
        return $info['quantity'];
    }
//return $info;
}

function update_inventory($db, $username) {

    $cart = get_cart($db, $username);

    foreach ($cart as $item) {

        $quantity_inventory = getQuantityAvailable($db, $item['upc']);
        $newQty = $quantity_inventory - $item['quantity'];
        $upc = $item['upc'];
        $query = "UPDATE inventory SET quantity = '$newQty' WHERE upc = '$upc' ";
        $statement = $db->prepare($query);
//$statement->bindValue(':upc', $upc);
        $statement->execute();
        $statement->closeCursor();
//return $newQty;
    }
}

function order_total($db, $username) {
    $total = 0;

    $cart = get_cart($db, $username);

    foreach ($cart as $item) {
        $info = get_item_info($db, $item['upc']);

        $total = $total + $item['quantity'] * $info['price'];
    }
    return $total;
}

function get_build_info($db, $upc) {
    $query = "SELECT * from inventory where upc = :upc";
    $statement = $db->prepare($query);
    $statement->bindValue(':upc', $upc);
    $statement->execute();
    $info = $statement->fetch(PDO::FETCH_ASSOC);
    $statement->closeCursor();
    return $info;
}

function get_item_info($db, $upc) {

    $query = "SELECT item_type from inventory where upc = :upc";
    $statement = $db->prepare($query);
    $statement->bindValue(':upc', $upc);
    $statement->execute();
    $info = $statement->fetch(PDO::FETCH_ASSOC);
    $statement->closeCursor();
    $item_table = "inventory_" . $info['item_type'];


    $query = "SELECT price,name from $item_table where upc = :upc";
    $statement = $db->prepare($query);
    $statement->bindValue(':upc', $upc);
    $statement->execute();
    $data = $statement->fetch(PDO::FETCH_ASSOC);
    $statement->closeCursor();
    return $data;
}

?>