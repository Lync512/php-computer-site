-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Apr 19, 2021 at 03:50 AM
-- Server version: 10.4.17-MariaDB
-- PHP Version: 8.0.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `computer_db`
--

-- --------------------------------------------------------


SET FOREIGN_KEY_CHECKS=OFF;
DROP TABLE IF EXISTS cart;
DROP TABLE IF EXISTS categories;
DROP TABLE IF EXISTS inventory;
DROP TABLE IF EXISTS inventory_case;
DROP TABLE IF EXISTS inventory_cpu;
DROP TABLE IF EXISTS inventory_cooler;
DROP TABLE IF EXISTS inventory_fan;
DROP TABLE IF EXISTS inventory_gfx;
DROP TABLE IF EXISTS inventory_keyboard;
DROP TABLE IF EXISTS inventory_mobo;
DROP TABLE IF EXISTS inventory_monitor;
DROP TABLE IF EXISTS inventory_mouse;
DROP TABLE IF EXISTS inventory_psu;
DROP TABLE IF EXISTS inventory_ram;
DROP TABLE IF EXISTS inventory_storage;
DROP TABLE IF EXISTS order_details;
DROP TABLE IF EXISTS orders;
DROP TABLE IF EXISTS users;
DROP TABLE IF EXISTS forum_topics;
DROP TABLE IF EXISTS user_info;
DROP TABLE IF EXISTS posts;
SET FOREIGN_KEY_CHECKS=ON;

-- --------------------------------------------------------

--
-- Table structure for table `cart`
--

CREATE TABLE `cart` (
  `upc` varchar(20) NOT NULL,
  `quantity` int(100) NOT NULL,
  `username` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `category` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`category`) VALUES
('AMD_CPU'),
('AMD_MOTHERBOARD'),
('AMD_RADEON_GRAPHICS'),
('AMD_WORKSTATION_COOLER'),
('AMD_WORKSTATION_CPU'),
('AMD_WORKSTATION_MOTHERBOARD'),
('CASE_FAN'),
('CPU_COOLER'),
('DESKTOP_CASE'),
('INTEL_CPU'),
('INTEL_MOTHERBOARD'),
('INTEL_WORKSTATION_COOLER'),
('INTEL_WORKSTATION_CPU'),
('INTEL_WORKSTATION_MOTHERBOARD'),
('KEYBOARD'),
('MONITOR'),
('MOUSE'),
('NVIDIA_GEFORCE_GRAPHICS'),
('NVIDIA_QUADRO_GRAPHICS'),
('PSU'),
('RAM'),
('STORAGE_DRIVE');

-- --------------------------------------------------------

--
-- Table structure for table `forum_topics`
--

CREATE TABLE `forum_topics` (
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `inventory`
--

CREATE TABLE `inventory` (
  `upc` varchar(20) NOT NULL,
  `quantity` int(100) NOT NULL,
  `item_type` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `inventory`
--

INSERT INTO `inventory` (`upc`, `quantity`, `item_type`) VALUES
('acpu001', 100, 'cpu'),
('acpu002', 100, 'cpu'),
('acpu003', 100, 'cpu'),
('acpu004', 100, 'cpu'),
('amobo001', 100, 'mobo'),
('amobo002', 100, 'mobo'),
('amobo003', 100, 'mobo'),
('amobo004', 100, 'mobo'),
('awscpu001', 100, 'cpu'),
('awscpu002', 100, 'cpu'),
('awscpu003', 100, 'cpu'),
('awscpu004', 100, 'cpu'),
('awsmobo001', 100, 'mobo'),
('awsmobo002', 100, 'mobo'),
('awsmobo003', 100, 'mobo'),
('cc001', 100, 'case'),
('cc002', 100, 'case'),
('cc003', 100, 'case'),
('cool001', 100, 'cooler'),
('cool002', 100, 'cooler'),
('cool003', 100, 'cooler'),
('cool004', 100, 'cooler'),
('coolaws001', 100, 'cooler'),
('coolaws002', 100, 'cooler'),
('cooliws001', 100, 'cooler'),
('cooliws002', 100, 'cooler'),
('dmon002', 100, 'monitor'),
('drv001', 100, 'storage'),
('drv002', 100, 'storage'),
('drv003', 100, 'storage'),
('drv004', 100, 'storage'),
('drv005', 100, 'storage'),
('drv006', 100, 'storage'),
('drv007', 100, 'storage'),
('drv008', 100, 'storage'),
('drv009', 100, 'storage'),
('fan001', 100, 'fan'),
('fan002', 100, 'fan'),
('gfxnvda001', 100, 'gfx'),
('gfxnvda002', 100, 'gfx'),
('gfxnvda003', 100, 'gfx'),
('gfxnvda004', 100, 'gfx'),
('gfxnvda005', 100, 'gfx'),
('gfxqnvda001', 100, 'gfx'),
('gfxqnvda002', 100, 'gfx'),
('gfxqnvda003', 100, 'gfx'),
('gfxqnvda004', 100, 'gfx'),
('gfxqnvda005', 100, 'gfx'),
('gfxrdn001', 100, 'gfx'),
('gfxrdn002', 100, 'gfx'),
('gfxrdn003', 100, 'gfx'),
('gfxrdn004', 100, 'gfx'),
('gfxrdnp001', 100, 'gfx'),
('gfxrdnp002', 100, 'gfx'),
('gfxrdnp003', 100, 'gfx'),
('icpu001', 100, 'cpu'),
('icpu002', 100, 'cpu'),
('icpu003', 100, 'cpu'),
('icpu004', 100, 'cpu'),
('imobo001', 100, 'mobo'),
('imobo002', 100, 'mobo'),
('imobo003', 100, 'mobo'),
('imobo004', 100, 'mobo'),
('imobo005', 100, 'mobo'),
('iwscpu001', 100, 'cpu'),
('iwscpu0010', 100, 'cpu'),
('iwscpu002', 100, 'cpu'),
('iwscpu003', 100, 'cpu'),
('iwscpu004', 100, 'cpu'),
('iwscpu005', 100, 'cpu'),
('iwscpu006', 100, 'cpu'),
('iwscpu007', 100, 'cpu'),
('iwscpu008', 100, 'cpu'),
('iwscpu009', 100, 'cpu'),
('iwsdmobo001', 100, 'mobo'),
('iwsdmobo002', 100, 'mobo'),
('iwsdmobo003', 100, 'mobo'),
('iwssmobo001', 100, 'mobo'),
('iwssmobo002', 100, 'mobo'),
('iwssmobo003', 100, 'mobo'),
('keyb001', 100, 'keyboard'),
('keyb002', 100, 'keyboard'),
('keyb003', 100, 'keyboard'),
('mous001', 100, 'mouse'),
('mous002', 100, 'mouse'),
('mous003', 100, 'mouse'),
('mous004', 100, 'mouse'),
('psu1000', 100, 'psu'),
('psu1300', 100, 'psu'),
('psu1600', 100, 'psu'),
('psu650', 100, 'psu'),
('psu750', 100, 'psu'),
('psu850', 100, 'psu'),
('qmon004', 100, 'monitor'),
('ram001', 100, 'ram'),
('ram002', 100, 'ram'),
('ram003', 100, 'ram'),
('ram004', 100, 'ram'),
('ram005', 100, 'ram'),
('ram006', 100, 'ram'),
('smon001', 100, 'monitor'),
('tmon003', 100, 'monitor'),
('wsram007', 100, 'ram'),
('wsram008', 100, 'ram'),
('wsram009', 100, 'ram');

-- --------------------------------------------------------

--
-- Table structure for table `inventory_case`
--

CREATE TABLE `inventory_case` (
  `upc` varchar(20) NOT NULL,
  `category` varchar(50) NOT NULL,
  `price` int(100) NOT NULL,
  `description` text NOT NULL,
  `name` varchar(80) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `inventory_case`
--

INSERT INTO `inventory_case` (`upc`, `category`, `price`, `description`, `name`) VALUES
('cc001', 'DESKTOP_CASE', 75, 'Compact ATX Mid tower computer case', 'Compact ATX Midtower'),
('cc002', 'DESKTOP_CASE', 100, 'Standard ATX midtower computer case', 'ATX midtower'),
('cc003', 'DESKTOP_CASE', 200, 'ATX Fulltower computer case', 'ATX Fulltower');

-- --------------------------------------------------------

--
-- Table structure for table `inventory_cooler`
--

CREATE TABLE `inventory_cooler` (
  `upc` varchar(20) NOT NULL,
  `category` varchar(50) NOT NULL,
  `price` int(100) NOT NULL,
  `description` text NOT NULL,
  `name` varchar(80) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `inventory_cooler`
--

INSERT INTO `inventory_cooler` (`upc`, `category`, `price`, `description`, `name`) VALUES
('cool001', 'CPU_COOLER', 100, '140mm dual tower air cooler', 'Noctua NH-D15'),
('cool002', 'CPU_COOLER', 70, '140mm single tower air cooler', 'Noctua NH-U14S'),
('cool003', 'CPU_COOLER', 99, '120mm single tower dual fan air cooler', 'Noctua NH-U12A'),
('cool004', 'CPU_COOLER', 65, '120mm single tower single fan air cooler', 'Noctua NH-U12S'),
('coolaws001', 'AMD_WORKSTATION_COOLER', 79, 'AMD Workstation 120mm air cooler', 'Noctua NH-U12S TR4-SP3'),
('coolaws002', 'AMD_WORKSTATION_COOLER', 89, 'AMD Workstation 140mm air cooler', 'Noctua NH-U14S TR4-SP3'),
('cooliws001', 'INTEL_WORKSTATION_COOLER', 109, 'Intel Workstation 120mm air cooler', 'Noctua NH-U12S DX-3647'),
('cooliws002', 'INTEL_WORKSTATION_COOLER', 99, 'Intel Workstation 140mm air cooler', 'Noctua NH-U14S DX-3647');

-- --------------------------------------------------------

--
-- Table structure for table `inventory_cpu`
--

CREATE TABLE `inventory_cpu` (
  `upc` varchar(20) NOT NULL,
  `category` varchar(50) NOT NULL,
  `price` int(100) NOT NULL,
  `description` varchar(500) NOT NULL,
  `name` varchar(80) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `inventory_cpu`
--

INSERT INTO `inventory_cpu` (`upc`, `category`, `price`, `description`, `name`) VALUES
('acpu001', 'AMD_CPU', 299, 'AMD 6 core processor', 'AMD Ryzen 5 5600X'),
('acpu002', 'AMD_CPU', 449, 'AMD 8 core processor', 'AMD Ryzen 7 5800X'),
('acpu003', 'AMD_CPU', 549, 'AMD 12 core processor', 'AMD Ryzen 9 5900X'),
('acpu004', 'AMD_CPU', 799, 'AMD 16 core processor', 'AMD Ryzen 9 5950X'),
('awscpu001', 'AMD_WORKSTATION_CPU', 1148, 'AMD 16 core workstation processor', 'AMD Ryzen Threadripper PRO 3955WX'),
('awscpu002', 'AMD_WORKSTATION_CPU', 2748, 'AMD 32 core workstation processor', 'AMD Ryzen Threadripper PRO 3975WX'),
('awscpu003', 'AMD_WORKSTATION_CPU', 5488, 'AMD 64 core workstation processor', 'AMD Ryzen Threadripper PRO 3995WX'),
('icpu001', 'INTEL_CPU', 154, 'Intel 4 core processor', 'Intel Core i3-11320'),
('icpu002', 'INTEL_CPU', 262, 'Intel 6 core processor', 'Intel Core i5-11600K'),
('icpu003', 'INTEL_CPU', 399, 'Intel 8 core processor', 'Intel Core i7-11700K'),
('icpu004', 'INTEL_CPU', 539, 'Intel 8 core processor', 'Intel Core i9-11900K'),
('iwscpu001', 'INTEL_WORKSTATION_CPU', 2612, 'Intel 28 core workstation processor', 'Intel Xeon Gold 6238R'),
('iwscpu0010', 'INTEL_WORKSTATION_CPU', 306, 'Intel 8 core workstation processor', 'Intel Xeon Bronze 33206R'),
('iwscpu002', 'INTEL_WORKSTATION_CPU', 1894, 'Intel 26 core workstation processor', 'Intel Xeon Gold 6230R'),
('iwscpu003', 'INTEL_WORKSTATION_CPU', 2200, 'Intel 24 core workstation processor', 'Intel Xeon Gold 6240R'),
('iwscpu004', 'INTEL_WORKSTATION_CPU', 2612, 'Intel 22 core workstation processor', 'Intel Xeon Gold 6238'),
('iwscpu005', 'INTEL_WORKSTATION_CPU', 1273, 'Intel 20 core workstation processor', 'Intel Xeon Gold 5218R'),
('iwscpu006', 'INTEL_WORKSTATION_CPU', 1555, 'Intel 18 core workstation processor', 'Intel Xeon Gold 5220'),
('iwscpu007', 'INTEL_WORKSTATION_CPU', 1300, 'Intel 16 core workstation processor', 'Intel Xeon Gold 6226R'),
('iwscpu008', 'INTEL_WORKSTATION_CPU', 694, 'Intel 12 core workstation processor', 'Intel Xeon Silver 4214R'),
('iwscpu009', 'INTEL_WORKSTATION_CPU', 501, 'Intel 10 core workstation processor', 'Intel Xeon Silver 4210R');

-- --------------------------------------------------------

--
-- Table structure for table `inventory_fan`
--

CREATE TABLE `inventory_fan` (
  `upc` varchar(20) NOT NULL,
  `category` varchar(50) NOT NULL,
  `price` int(100) NOT NULL,
  `description` text NOT NULL,
  `name` varchar(80) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `inventory_fan`
--

INSERT INTO `inventory_fan` (`upc`, `category`, `price`, `description`, `name`) VALUES
('fan001', 'CASE_FAN', 15, '120mm Case fan', '120mm Case fan'),
('fan002', 'CASE_FAN', 25, '140mm Case fan', '140mm Case fan');

-- --------------------------------------------------------

--
-- Table structure for table `inventory_gfx`
--

CREATE TABLE `inventory_gfx` (
  `upc` varchar(20) NOT NULL,
  `category` varchar(50) NOT NULL,
  `price` int(50) NOT NULL,
  `description` text NOT NULL,
  `name` varchar(80) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `inventory_gfx`
--

INSERT INTO `inventory_gfx` (`upc`, `category`, `price`, `description`, `name`) VALUES
('gfxnvda001', 'NVIDIA_GEFORCE_GRAPHICS', 329, 'Nvidia GeForce RTX Graphics card', 'RTX 3060'),
('gfxnvda002', 'NVIDIA_GEFORCE_GRAPHICS', 399, 'Nvidia GeForce RTX Graphics card', 'RTX 3060ti'),
('gfxnvda003', 'NVIDIA_GEFORCE_GRAPHICS', 499, 'Nvidia GeForce RTX Graphics card', 'RTX 3070'),
('gfxnvda004', 'NVIDIA_GEFORCE_GRAPHICS', 699, 'Nvidia GeForce RTX Graphics card', 'RTX 3080'),
('gfxnvda005', 'NVIDIA_GEFORCE_GRAPHICS', 1499, 'Nvidia GeForce RTX Graphics card', 'RTX 3090'),
('gfxqnvda001', 'NVIDIA_QUADRO_GRAPHICS', 999, 'Nvidia Quadro RTX Graphics card', 'Quadro RTX 4000'),
('gfxqnvda002', 'NVIDIA_QUADRO_GRAPHICS', 1918, 'Nvidia Quadro RTX Graphics card', 'Quadro RTX 5000'),
('gfxqnvda003', 'NVIDIA_QUADRO_GRAPHICS', 3999, 'Nvidia Quadro RTX Graphics card', 'Quadro RTX 6000'),
('gfxqnvda004', 'NVIDIA_QUADRO_GRAPHICS', 5277, 'Nvidia Quadro RTX Graphics card', 'Quadro RTX 8000'),
('gfxqnvda005', 'NVIDIA_QUADRO_GRAPHICS', 5500, 'Nvidia RTX Workstation Graphics card', 'Nvidia RTX A6000'),
('gfxrdn001', 'AMD_RADEON_GRAPHICS', 400, 'AMD Radeon Graphics card', 'AMD Radeon RX 6700XT'),
('gfxrdn002', 'AMD_RADEON_GRAPHICS', 579, 'AMD Radeon Graphics card', 'AMD Radeon RX 6800'),
('gfxrdn003', 'AMD_RADEON_GRAPHICS', 649, 'AMD Radeon Graphics card', 'AMD Radeon RX 6800XT'),
('gfxrdn004', 'AMD_RADEON_GRAPHICS', 999, 'AMD Radeon Graphics card', 'AMD Radeon RX 6900XT'),
('gfxrdnp001', 'AMD_RADEON_GRAPHICS', 499, 'AMD Radeon Pro Graphics card', 'AMD Radeon Pro W5500'),
('gfxrdnp002', 'AMD_RADEON_GRAPHICS', 999, 'AMD Radeon Pro Graphics card', 'AMD Radeon Pro W5700'),
('gfxrdnp003', 'AMD_RADEON_GRAPHICS', 1899, 'AMD Radeon Pro Graphics card', 'AMD Radeon Pro VII');

-- --------------------------------------------------------

--
-- Table structure for table `inventory_keyboard`
--

CREATE TABLE `inventory_keyboard` (
  `upc` varchar(20) NOT NULL,
  `category` varchar(50) NOT NULL,
  `price` int(100) NOT NULL,
  `description` text NOT NULL,
  `name` varchar(80) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `inventory_keyboard`
--

INSERT INTO `inventory_keyboard` (`upc`, `category`, `price`, `description`, `name`) VALUES
('keyb001', 'KEYBOARD', 170, '104 key mechanical keyboard', '104-key Mechanical keyboard MX Blue'),
('keyb002', 'KEYBOARD', 165, '87 key mechanical keyboard', '87-key Mechanical keyboard MX Blue'),
('keyb003', 'KEYBOARD', 99, 'Wireless non mechanical keyboard', 'Logitech MX Keys wireless keyboard');

-- --------------------------------------------------------

--
-- Table structure for table `inventory_mobo`
--

CREATE TABLE `inventory_mobo` (
  `upc` varchar(20) NOT NULL,
  `category` varchar(50) NOT NULL,
  `price` int(100) NOT NULL,
  `description` text NOT NULL,
  `name` varchar(80) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `inventory_mobo`
--

INSERT INTO `inventory_mobo` (`upc`, `category`, `price`, `description`, `name`) VALUES
('amobo001', 'AMD_MOTHERBOARD', 150, 'AMD X570 Desktop motherboard', 'Asus X570 Motherboard'),
('amobo002', 'AMD_MOTHERBOARD', 180, 'AMD X570 Desktop motherboard', 'Gigabyte X570 Motherboard'),
('amobo003', 'AMD_MOTHERBOARD', 130, 'AMD X570 Desktop motherboard', 'MSI X570 Motherboard'),
('amobo004', 'AMD_MOTHERBOARD', 120, 'AMD X570 Desktop motherboard', 'ASrock X570 Motherboard'),
('awsmobo001', 'AMD_WORKSTATION_MOTHERBOARD', 1044, 'AMD WRX80 Workstation Motherboard', 'Asus WRX80 Motherboard'),
('awsmobo002', 'AMD_WORKSTATION_MOTHERBOARD', 650, 'AMD WRX80 Motherboard', 'Supermicro WRX80 Motherboard'),
('awsmobo003', 'AMD_WORKSTATION_MOTHERBOARD', 700, 'AMD WRX80 Motherboard', 'Gigabyte WRX80 Motherboard'),
('imobo001', 'INTEL_MOTHERBOARD', 280, 'Intel Z590 Desktop Motherboard', 'Asus Z590 Motherboard'),
('imobo002', 'INTEL_MOTHERBOARD', 250, 'Intel Z590 Desktop Motherboard', 'Gigabyte Z590 Motherboard'),
('imobo003', 'INTEL_MOTHERBOARD', 230, 'Intel Z590 Desktop Motherboard', 'MSI Z590 Motherboard'),
('imobo004', 'INTEL_MOTHERBOARD', 200, 'Intel Z590 Desktop Motherboard', 'ASrock Z590 Motherboard'),
('iwsdmobo001', 'INTEL_WORKSTATION_MOTHERBOARD', 1200, 'Intel C621 Dual socket workstation motherboard', 'Asus C621 dual socket'),
('iwsdmobo002', 'INTEL_WORKSTATION_MOTHERBOARD', 1000, 'Intel C621 Dual socket workstation motherboard', 'Gigabyte C621 dual socket'),
('iwsdmobo003', 'INTEL_WORKSTATION_MOTHERBOARD', 800, 'Intel C621 Dual socket workstation motherboard', 'Supermicro C621 dual socket'),
('iwssmobo001', 'INTEL_WORKSTATION_MOTHERBOARD', 500, 'Intel C621 Single socket workstation motherboard', 'Asus C621 Single socket'),
('iwssmobo002', 'INTEL_WORKSTATION_MOTHERBOARD', 500, 'Intel C621 Single socket workstation motherboard', 'Gigabyte C621 Single socket'),
('iwssmobo003', 'INTEL_WORKSTATION_MOTHERBOARD', 500, 'Intel C621 Single socket workstation motherboard', 'Supermicro C621 Single socket');

-- --------------------------------------------------------

--
-- Table structure for table `inventory_monitor`
--

CREATE TABLE `inventory_monitor` (
  `upc` varchar(20) NOT NULL,
  `category` varchar(50) NOT NULL,
  `price` int(100) NOT NULL,
  `description` text NOT NULL,
  `name` varchar(80) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `inventory_monitor`
--

INSERT INTO `inventory_monitor` (`upc`, `category`, `price`, `description`, `name`) VALUES
('dmon002', 'MONITOR', 218, 'Dual monitor stand included', 'Dual monitor kit'),
('qmon004', 'MONITOR', 436, 'Quad monitor stand included', 'Quad monitor kit'),
('smon001', 'MONITOR', 109, 'Single monitor', 'Single monitor'),
('tmon003', 'MONITOR', 327, 'Triple monitor stand included', 'Triple monitor kit');

-- --------------------------------------------------------

--
-- Table structure for table `inventory_mouse`
--

CREATE TABLE `inventory_mouse` (
  `upc` varchar(20) NOT NULL,
  `category` varchar(50) NOT NULL,
  `price` int(100) NOT NULL,
  `description` text NOT NULL,
  `name` varchar(80) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `inventory_mouse`
--

INSERT INTO `inventory_mouse` (`upc`, `category`, `price`, `description`, `name`) VALUES
('mous001', 'MOUSE', 68, 'Wired gaming mouse', 'Razer DeathAdder v2 gaming mouse'),
('mous002', 'MOUSE', 119, 'Wireless gaming mouse', 'Logitech G502 Lightspeed wireless gaming mouse'),
('mous003', 'MOUSE', 99, 'Wireless mouse', 'Logitech MX Master 3'),
('mous004', 'MOUSE', 86, 'Wireless trackball mouse', 'Logitech MX Ergo');

-- --------------------------------------------------------

--
-- Table structure for table `inventory_psu`
--

CREATE TABLE `inventory_psu` (
  `upc` varchar(20) NOT NULL,
  `category` varchar(50) NOT NULL,
  `price` int(100) NOT NULL,
  `description` varchar(500) NOT NULL,
  `name` varchar(80) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `inventory_psu`
--

INSERT INTO `inventory_psu` (`upc`, `category`, `price`, `description`, `name`) VALUES
('psu1000', 'PSU', 349, '1000w 80+ Titanium Modular ATX Power supply', 'EVGA SuperNOVA 1000 T2'),
('psu1600', 'PSU', 579, '1600w 80+ Titanium Modular ATX Power supply', 'EVGA SuperNOVA 1600 T2'),
('psu650', 'PSU', 129, '650W 80+ Gold Modular ATX Power supply', 'EVGA SuperNOVA 650 GA'),
('psu750', 'PSU', 139, '750W 80+ Gold Modular ATX Power supply', 'EVGA SuperNOVA 750 GA'),
('psu850', 'PSU', 239, '850W 80+ Platinum Modular ATX Power supply', 'EVGA SuperNOVA 850 P2');

-- --------------------------------------------------------

--
-- Table structure for table `inventory_ram`
--

CREATE TABLE `inventory_ram` (
  `upc` varchar(20) NOT NULL,
  `category` varchar(50) NOT NULL,
  `price` int(100) NOT NULL,
  `description` text NOT NULL,
  `name` varchar(80) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `inventory_ram`
--

INSERT INTO `inventory_ram` (`upc`, `category`, `price`, `description`, `name`) VALUES
('ram001', 'RAM', 50, 'RAM 8GB', '8Gb RAM kit'),
('ram002', 'RAM', 60, 'RAM 16GB', '16Gb RAM kit'),
('ram003', 'RAM', 130, 'RAM 32GB', '32Gb RAM kit'),
('ram004', 'RAM', 150, 'RAM 48GB', '48Gb RAM kit'),
('ram005', 'RAM', 300, 'RAM 64GB', '64Gb RAM kit'),
('ram006', 'RAM', 500, 'RAM 128GB', '128Gb RAM kit'),
('wsram007', 'RAM', 1000, 'RAM 256GB', '256Gb RAM kit'),
('wsram008', 'RAM', 1200, 'RAM 512GB', '512Gb RAM kit'),
('wsram009', 'RAM', 1500, 'RAM 1024GB', '1.5Tb RAM kit');

-- --------------------------------------------------------

--
-- Table structure for table `inventory_storage`
--

CREATE TABLE `inventory_storage` (
  `upc` varchar(20) NOT NULL,
  `category` varchar(50) NOT NULL,
  `price` int(100) NOT NULL,
  `description` text NOT NULL,
  `name` varchar(80) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `inventory_storage`
--

INSERT INTO `inventory_storage` (`upc`, `category`, `price`, `description`, `name`) VALUES
('drv001', 'STORAGE_DRIVE', 79, '250GB NVMe SSD', 'Samsung 980 PRO NVMe SSD 250GB'),
('drv002', 'STORAGE_DRIVE', 119, '500GB NVMe SSD', 'Samsung 980 PRO NVMe SSD 500GB'),
('drv003', 'STORAGE_DRIVE', 199, '1TB NVMe SSD', 'Samsung 980 PRO NVMe SSD 1TB'),
('drv004', 'STORAGE_DRIVE', 379, '2TB NVMe SSD', 'Samsung 980 PRO NVMe SSD 2TB'),
('drv005', 'STORAGE_DRIVE', 40, '250GB SATA SSD', 'Samsung 870 EVO SATA 2.5\" SSD 250GB'),
('drv006', 'STORAGE_DRIVE', 70, '500GB SATA SSD', 'Samsung 870 EVO SATA 2.5\" SSD 500GB'),
('drv007', 'STORAGE_DRIVE', 119, '1TB SATA SSD', 'Samsung 870 EVO SATA 2.5\" SSD 1TB'),
('drv008', 'STORAGE_DRIVE', 229, '2TB SATA SSD', 'Samsung 870 EVO SATA 2.5\" SSD 2TB'),
('drv009', 'STORAGE_DRIVE', 479, '4TB SATA SSD', 'Samsung 870 EVO SATA 2.5\" SSD 4TB');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `username` varchar(50) NOT NULL,
  `order_number` int(100) NOT NULL,
  `date` date NOT NULL,
  `total` int(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `order_details`
--

CREATE TABLE `order_details` (
  `order_number` int(100) NOT NULL,
  `upc` varchar(50) NOT NULL,
  `price` int(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `post_id` int(8) NOT NULL,
  `post_content` text NOT NULL,
  `post_date` datetime NOT NULL DEFAULT current_timestamp(),
  `post_topic` varchar(255) NOT NULL,
  `post_by` int(8) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` int(8) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `user_info`
--

CREATE TABLE `user_info` (
  `username` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `address` varchar(50) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cart`
--
ALTER TABLE `cart`
  ADD PRIMARY KEY (`upc`,`username`),
  ADD KEY `cart_ibfk_2` (`username`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`category`);

--
-- Indexes for table `forum_topics`
--
ALTER TABLE `forum_topics`
  ADD PRIMARY KEY (`name`);

--
-- Indexes for table `inventory`
--
ALTER TABLE `inventory`
  ADD PRIMARY KEY (`upc`);

--
-- Indexes for table `inventory_case`
--
ALTER TABLE `inventory_case`
  ADD PRIMARY KEY (`upc`),
  ADD KEY `inventory_case_ibfk_2` (`category`);

--
-- Indexes for table `inventory_cooler`
--
ALTER TABLE `inventory_cooler`
  ADD PRIMARY KEY (`upc`),
  ADD KEY `category` (`category`);

--
-- Indexes for table `inventory_cpu`
--
ALTER TABLE `inventory_cpu`
  ADD PRIMARY KEY (`upc`),
  ADD KEY `inventory_cpu_ibfk_2` (`category`);

--
-- Indexes for table `inventory_fan`
--
ALTER TABLE `inventory_fan`
  ADD PRIMARY KEY (`upc`),
  ADD KEY `inventory_fan_ibfk_2` (`category`);

--
-- Indexes for table `inventory_gfx`
--
ALTER TABLE `inventory_gfx`
  ADD PRIMARY KEY (`upc`),
  ADD KEY `inventory_gfx_ibfk_2` (`category`);

--
-- Indexes for table `inventory_keyboard`
--
ALTER TABLE `inventory_keyboard`
  ADD PRIMARY KEY (`upc`),
  ADD KEY `inventory_keyboard_ibfk_2` (`category`);

--
-- Indexes for table `inventory_mobo`
--
ALTER TABLE `inventory_mobo`
  ADD PRIMARY KEY (`upc`),
  ADD KEY `inventory_mobo_ibfk_2` (`category`);

--
-- Indexes for table `inventory_monitor`
--
ALTER TABLE `inventory_monitor`
  ADD PRIMARY KEY (`upc`),
  ADD KEY `inventory_monitor_ibfk_2` (`category`);

--
-- Indexes for table `inventory_mouse`
--
ALTER TABLE `inventory_mouse`
  ADD PRIMARY KEY (`upc`),
  ADD KEY `inventory_mouse_ibfk_2` (`category`);

--
-- Indexes for table `inventory_psu`
--
ALTER TABLE `inventory_psu`
  ADD PRIMARY KEY (`upc`),
  ADD KEY `inventory_psu_ibfk_2` (`category`);

--
-- Indexes for table `inventory_ram`
--
ALTER TABLE `inventory_ram`
  ADD PRIMARY KEY (`upc`),
  ADD KEY `inventory_ram_ibfk_2` (`category`);

--
-- Indexes for table `inventory_storage`
--
ALTER TABLE `inventory_storage`
  ADD PRIMARY KEY (`upc`),
  ADD KEY `inventory_storage_ibfk_2` (`category`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`order_number`),
  ADD KEY `username` (`username`);

--
-- Indexes for table `order_details`
--
ALTER TABLE `order_details`
  ADD PRIMARY KEY (`order_number`,`upc`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`post_id`),
  ADD KEY `post_topic` (`post_topic`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `user_info`
--
ALTER TABLE `user_info`
  ADD KEY `username` (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `post_id` int(8) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(8) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `cart`
--
ALTER TABLE `cart`
  ADD CONSTRAINT `cart_ibfk_1` FOREIGN KEY (`upc`) REFERENCES `inventory` (`upc`);

--
-- Constraints for table `inventory_case`
--
ALTER TABLE `inventory_case`
  ADD CONSTRAINT `inventory_case_ibfk_1` FOREIGN KEY (`upc`) REFERENCES `inventory` (`upc`),
  ADD CONSTRAINT `inventory_case_ibfk_2` FOREIGN KEY (`category`) REFERENCES `categories` (`category`);

--
-- Constraints for table `inventory_cooler`
--
ALTER TABLE `inventory_cooler`
  ADD CONSTRAINT `inventory_cooler_ibfk_1` FOREIGN KEY (`upc`) REFERENCES `inventory` (`upc`),
  ADD CONSTRAINT `inventory_cooler_ibfk_2` FOREIGN KEY (`category`) REFERENCES `categories` (`category`);

--
-- Constraints for table `inventory_cpu`
--
ALTER TABLE `inventory_cpu`
  ADD CONSTRAINT `inventory_cpu_ibfk_1` FOREIGN KEY (`upc`) REFERENCES `inventory` (`upc`),
  ADD CONSTRAINT `inventory_cpu_ibfk_2` FOREIGN KEY (`category`) REFERENCES `categories` (`category`);

--
-- Constraints for table `inventory_fan`
--
ALTER TABLE `inventory_fan`
  ADD CONSTRAINT `inventory_fan_ibfk_1` FOREIGN KEY (`upc`) REFERENCES `inventory` (`upc`),
  ADD CONSTRAINT `inventory_fan_ibfk_2` FOREIGN KEY (`category`) REFERENCES `categories` (`category`);

--
-- Constraints for table `inventory_gfx`
--
ALTER TABLE `inventory_gfx`
  ADD CONSTRAINT `inventory_gfx_ibfk_1` FOREIGN KEY (`upc`) REFERENCES `inventory` (`upc`),
  ADD CONSTRAINT `inventory_gfx_ibfk_2` FOREIGN KEY (`category`) REFERENCES `categories` (`category`);

--
-- Constraints for table `inventory_keyboard`
--
ALTER TABLE `inventory_keyboard`
  ADD CONSTRAINT `inventory_keyboard_ibfk_1` FOREIGN KEY (`upc`) REFERENCES `inventory` (`upc`),
  ADD CONSTRAINT `inventory_keyboard_ibfk_2` FOREIGN KEY (`category`) REFERENCES `categories` (`category`);

--
-- Constraints for table `inventory_mobo`
--
ALTER TABLE `inventory_mobo`
  ADD CONSTRAINT `inventory_mobo_ibfk_1` FOREIGN KEY (`upc`) REFERENCES `inventory` (`upc`),
  ADD CONSTRAINT `inventory_mobo_ibfk_2` FOREIGN KEY (`category`) REFERENCES `categories` (`category`);

--
-- Constraints for table `inventory_monitor`
--
ALTER TABLE `inventory_monitor`
  ADD CONSTRAINT `inventory_monitor_ibfk_1` FOREIGN KEY (`upc`) REFERENCES `inventory` (`upc`),
  ADD CONSTRAINT `inventory_monitor_ibfk_2` FOREIGN KEY (`category`) REFERENCES `categories` (`category`);

--
-- Constraints for table `inventory_mouse`
--
ALTER TABLE `inventory_mouse`
  ADD CONSTRAINT `inventory_mouse_ibfk_1` FOREIGN KEY (`upc`) REFERENCES `inventory` (`upc`),
  ADD CONSTRAINT `inventory_mouse_ibfk_2` FOREIGN KEY (`category`) REFERENCES `categories` (`category`);

--
-- Constraints for table `inventory_psu`
--
ALTER TABLE `inventory_psu`
  ADD CONSTRAINT `inventory_psu_ibfk_1` FOREIGN KEY (`upc`) REFERENCES `inventory` (`upc`),
  ADD CONSTRAINT `inventory_psu_ibfk_2` FOREIGN KEY (`category`) REFERENCES `categories` (`category`);

--
-- Constraints for table `inventory_ram`
--
ALTER TABLE `inventory_ram`
  ADD CONSTRAINT `inventory_ram_ibfk_1` FOREIGN KEY (`upc`) REFERENCES `inventory` (`upc`),
  ADD CONSTRAINT `inventory_ram_ibfk_2` FOREIGN KEY (`category`) REFERENCES `categories` (`category`);

--
-- Constraints for table `inventory_storage`
--
ALTER TABLE `inventory_storage`
  ADD CONSTRAINT `inventory_storage_ibfk_1` FOREIGN KEY (`upc`) REFERENCES `inventory` (`upc`),
  ADD CONSTRAINT `inventory_storage_ibfk_2` FOREIGN KEY (`category`) REFERENCES `categories` (`category`);

--
-- Constraints for table `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `orders_ibfk_1` FOREIGN KEY (`username`) REFERENCES `users` (`username`);

--
-- Constraints for table `order_details`
--
ALTER TABLE `order_details`
  ADD CONSTRAINT `order_details_ibfk_1` FOREIGN KEY (`order_number`) REFERENCES `orders` (`order_number`);

--
-- Constraints for table `user_info`
--
ALTER TABLE `user_info`
  ADD CONSTRAINT `user_info_ibfk_1` FOREIGN KEY (`username`) REFERENCES `users` (`username`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
