<?php
session_start();
require_once('../includes/open_db.php');
$pageTitle = "CPU";
include('../includes/header.php');
include('../includes/functions.php');

//debug error reporting
error_reporting(E_ALL);
ini_set("display_errors", 1);

if (!isset($_SESSION['current_user'])) {
    echo "<script type='text/javascript'>
        alert('You must be logged in');
    location='../index.php';
    
</script>";
}

if (isset($_POST['AMD_Desktop'])) {
    $_SESSION["cpu"] = "AMD_CPU";
    $_SESSION["mobo"] = "AMD_MOTHERBOARD";
    $_SESSION["cooler"] = "CPU_COOLER";
}
if (isset($_POST['AMD_Workstation'])) {
    $_SESSION["cpu"] = "AMD_WORKSTATION_CPU";
    $_SESSION["mobo"] = "AMD_WORKSTATION_MOTHERBOARD";
    $_SESSION["cooler"] = "AMD_WORKSTATION_COOLER";
}
if (isset($_POST['Intel_Desktop'])) {
    $_SESSION["cpu"] = "INTEL_CPU";
    $_SESSION["mobo"] = "INTEL_MOTHERBOARD";
    $_SESSION["cooler"] = "CPU_COOLER";
}
if (isset($_POST['Intel_Workstation'])) {
    $_SESSION["cpu"] = "INTEL_WORKSTATION_CPU";
    $_SESSION["mobo"] = "INTEL_WORKSTATION_MOTHERBOARD";
    $_SESSION["cooler"] = "INTEL_WORKSTATION_COOLER";
}

echo $_SESSION['cpu'];
?>
<?php
//create hat array and cart array if not already done

$inventory = get_cpu_inventory($db, $_SESSION["cpu"]);
//print_r($inventory);
//print_r($_SESSION['cpu']);
//adding item to cart?


if (isset($_POST['upc'])) {
    //enforce one cpu per user
    //if in cart ask user if they want to remove and replace
    $quantity_inventory = getQuantityAvailable($db, $_POST['upc']);
    echo $_POST['upc'];
    echo $quantity_inventory;
    $quantity_cart = getQuantityInCart($db, $_POST['upc']);
    if ($quantity_inventory > $quantity_cart) {
        add_cart($db, $_POST['upc'], $_SESSION['current_user']);
        unset($_POST['upc']);
    } else {
        echo "<p id='emsg'>This item is out of stock. It cannot be added to the cart.</p>";
    }
}
?> 

<main class="flex_content">     <!-- needed so that main in other pages won't be styled as a flex box; could have used separate css pages to avoid this -->     
    <?php
//display each cpu in the inventory

    $inv_html = "";

    foreach ($inventory as $cpu_itm) {
        $inv_html = get_cpu_html($cpu_itm) . $inv_html;
    }

    echo $inv_html;
    ?> 
</main>

<form action="../build_pages/mobo.php" method="post" id="next1">
    <input type="submit" name="Next page" value="Next Page" class="nav_button">

</form>
<form action="../index.php" method="post" id="back">
    <input type="submit" name="GO back" value="Home" class="nav_button">
</form>
<?php include('../includes/footer.php') ?>