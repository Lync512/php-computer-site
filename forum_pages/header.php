<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>

        <title>Sync Computer Systems Forum</title>
        <meta charset="utf-8">
            <meta name="viewport" content="width=device-width, initial-scale=1">
                <link rel="stylesheet" href="../css/normalize.css">
                    <link rel="stylesheet" href="../css/forum_main.css">
                        <link rel="shortcut icon" href="../img/favicon/favicon.ico">
                            </head>
                            <body>
                                <h1>Sync Computer Systems forum</h1>
                                <div id="wrapper">
                                    <div id="menu">
                                        <a class="menu_item" href="../index.php">Site home</a>
                                        <a class="menu_item" href="../forum_pages/index.php">Forum home</a><!-- go home -->
                                        <a class="menu_item" href="../forum_pages/create_post.php">create a post</a><!-- create a topic -->
                                       
                                    </div>
                                    <div id="userbar">
                                        <?php
                                        if (isset($_SESSION['current_user'])) {
                                            echo 'Hello ' . $_SESSION['current_user'] . ' Not you? <a href="../login_files/logout.php">Sign out</a>';
                                        } else {
                                            echo '<a href="../login_files/login_start.php"> Sign in or create an account</a>';
                                        }
                                        ?>  
                                    </div>
                                    <div id="content">
