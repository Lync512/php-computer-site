<?php
//create_cat.php
session_start();
include '../includes/open_db.php';
include '../includes/functions.php';
include 'header.php';

error_reporting(E_ALL);
ini_set("display_errors", 1);

if (!isset($_SESSION['current_user'])) {
    echo "<script type='text/javascript'>
        alert('You must be logged in');
    location='../index.php';
    
</script>";
}


if (isset($_POST['create_post'])) {

    $topic_name = htmlspecialchars($_POST['new_topic']);
    if ($topic_name != "") {

        new_topic($db, $topic_name);

        $post_content = htmlspecialchars($_POST['post_content']);
        $post_by = get_user_id($db, $_SESSION['current_user']);
        new_post($db, $post_content, $topic_name, $post_by);
    } elseif (isset($_POST['topic_name'])) {

        $post_content = htmlspecialchars($_POST['post_content']);
        $post_by = get_user_id($db, $_SESSION['current_user']);
        new_post($db, $post_content, $topic_name, $post_by);
    }
}

echo '<h2>Post a comment</h2>';
//the form hasn't been posted yet, display it
//retrieve the categories from the database for use in the dropdown
?>
<form method="post" action="">

    <p>Enter a new topic or choose existing topic</p> 
    <input type="text" name="new_topic" placeholder="enter new topic here" >
    <br>
    Existing topics:
    <select name="topic_name">
        <br>
        <?php
        $topics = get_topics($db);

        //echo $row['cat_id'];
        //echo $row['cat_name'];
        $count = count($topics);


        for ($i = 0; $i < $count; $i++) {
            $id = $topics[$i]['id'];
            $name = $topics[$i]['name'];
            ?>
            <option value="<?php echo'$id'; ?>"><?php echo "$name"; ?></option>

            <?php
        }
        ?>
        <br></br>
    </select>
    <br></br>
    Message: 
    <br></br>
    <textarea name="post_content" ></textarea>
    <br></br>
    <input type="submit" value="Create post" name="create_post" >
</form>
<?php
include 'footer.php';
?>
