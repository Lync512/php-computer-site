<?php

session_start();
include 'header.php';
include '../includes/open_db.php';
include '../includes/functions.php';
//debug error reporting
error_reporting(E_ALL);
ini_set("display_errors", 1);


$posts = get_posts($db);
$columns = array_column($posts, 'post_topic');
array_multisort($columns, SORT_ASC, $posts);
$count = count($posts);

if ($count == 0) {
    echo 'The topics could not be displayed, please try again later.';
} else {

    //table time
    ?>
    <table border='1'>
        <tr>
            <th>Post</th><!-- category -->
        </tr>
    </table>

    <?php

    for ($i = 0; $i < $count; $i++) {
        echo '<tr>';
        echo '<td class="leftpart">';
        //  echo '<h3><a href="category.php?id">';
        echo "<br>";
        echo 'Post topic: ' . $posts[$i]['post_topic'];
        echo "<br>";
        echo '</a></h3>';
        echo '</td>';
       
        echo '<td class="rightpart">';
        echo 'Post content: ' . $posts[$i]['post_content'];
        echo "<br>";
        //  echo '<a href="category.php?id=">Topic subject</a>';
        echo '</td>';
        echo '</tr>';
    }
}

include 'footer.php';
?>
    
