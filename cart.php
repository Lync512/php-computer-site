<?php
session_start();
require_once('includes/open_db.php');
$pageTitle = "Payment Information";
include('includes/header_1.php');
include('includes/functions.php');

//debug error reporting 
error_reporting(E_ALL);
ini_set("display_errors", 1);


//only logged in users may access

if(!isset($_SESSION['current_user'])){
    echo "<script type='text/javascript'>
        alert('You must be logged in');
    location='index.php';
    
</script>";}


if (isset($_POST["cancel"])) {
    clear_cart($db, $_SESSION['current_user']);
}
//$user = $_SESSION['current_user'];
//echo $user;
$cart = get_cart($db, $_SESSION['current_user']);

if (count($cart) == 0) {
    echo '<section class="empty_cart"><p>Your cart is empty.</p>';
    echo '<a href="index.php"><input type="button" value="Continue Shopping"></a></section>';
} else {
    ?>

    <main> 

        <table>
            <thead>
                <tr>
                    <th colspan="4" id="table_title">Order Summary</th>
                </tr>
                <tr>
                    <th>Item</th>
                    <th>Quantity</th>
                    <th>Unit Price</th>
                    <th>Total Price</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $order_total = 0;

                foreach ($cart as $item) {
                    $build = get_item_info($db, $item['upc']);
                   
                    ?>
                    <tr>
                        <td><?php echo $build['name']; ?></td>
                        <td><?php echo $item['quantity']; ?></td>
                        <td><?php echo sprintf("$%.2f", $build['price']); ?></td>
                        <td><?php echo sprintf("$%.2f", $item['quantity'] * $build['price']); ?> </td>
                    </tr>
        <?php
        $order_total = order_total($db, $_SESSION['current_user']);
    }
    //$_SESSION['order_total'] = $order_total;
    ?> 


            </tbody>
            <tfoot>
                <tr>
                    <td>Order Total</td>
                    <td colspan="3" id='order_total'><?php echo sprintf("$%.2f", $order_total); ?></td>
                </tr>
            </tfoot>
        </table>


        <a href="index.php"><input type="button" value="Continue Shopping"></a>

        <form action="cart.php" method="post" id="cancel_form">
            <input type="submit" name="cancel" value="Cancel Order">

        </form>

        <a href="confirm.php"><input type="button" value="Place Order"></a>


    </main>

    <?php
} // end else

include('includes/footer.php');
?>


