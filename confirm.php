<?php
session_start();
require_once('includes/open_db.php');
$pageTitle = "Order Confirmation";
include('includes/header_1.php');
include('includes/functions.php');

//debug error reporting
error_reporting(E_ALL);
ini_set("display_errors", 1);


if(!isset($_SESSION['current_user'])){
    echo "<script type='text/javascript'>
        alert('You must be logged in');
    location='../index.php';
    
</script>";}

?>

<main class="confirm_notice">
    <h2>Order Confirmation</h2>

    <?php

    $order_total = order_total($db, $_SESSION['current_user']);

    $formatted_total = sprintf("$%.2f", $order_total);

    echo "<p>Your order was placed on " . date('F j' . ', ' . 'Y') . " at " . date('g:i a') . "</p>";

    echo "<p>Your order total is $formatted_total.</p>";
?>

    <a href="index.php"><input type="button" value="Continue Shopping" class="nav_button"></a>
    <?php
    update_inventory($db, $_SESSION['current_user']);
    clear_cart($db, $_SESSION['current_user']);
    ?>

</main>

<?php include('includes/footer.php') ?>