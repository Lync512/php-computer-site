### PHP Computer Builder

Computer designer site written in PHP, HTML & CSS with a PHPMyAdmin SQL backend

To run on a local system this must be used with an XAMPP or with another apache server. 

Additionally for proper use the database must first be imported and visible to the site code. 

This was written as a final project for a Web development class I took.
