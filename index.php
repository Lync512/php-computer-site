<!DOCTYPE html>
<?php
session_start();
require_once('includes/open_db.php');
$pageTitle = "Computer builder start";
include('includes/header_1.php');
include('includes/functions.php');

//debug error reporting
error_reporting(E_ALL);
ini_set("display_errors", 1);
?>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <header>
            <nav>
                <?php
                if(!isset($_SESSION['current_user'])){                             
                ?>
                    <ul><a href="login_files/login_start.php" alt="login" class="navbar"><h3>login</h3></a></ul>
                <?php
                }
                else{                  
                ?>
                    <ul><a href="login_files/logout.php" alt="login" class="navbar"><h3>logout</h3></a></ul>
                    <ul><a href="forum_pages/create_post.php" alt="forum" class="navbar"><h3>Forum</h3></a></ul>
                    
                <?php
                
                  }
                ?>
                <a href="cart.php"><img src="img/cart.png" alt="cart" class="navbar"></a>
            </nav>
        </header>
        <h2>Welcome to the PC Builder System</h2>
        <p>
            In the following pages you will assemble a computer parts list.
            All parts have been carefully selected for maximum compatibility and component reliability so you can build with confidence.
            We offer a selection of both AMD and Intel desktop and workstation parts to configure systems for every use case.
            From home computers, to gaming computers, to professional workstations. We offer parts for all systems. 
            Please enjoy your build process!
        </p>
        <p></p>
        <p>
            To start the build process please create an account and select which platform you wish to use:
        </p>
        
        <form action="build_pages/cpu.php" method="post" id="build-start">
            <input type="submit" name="AMD_Desktop" value="AMD_Desktop" class="nav_button">
            <input type="submit" name="AMD_Workstation" value="AMD_Workstation" class="nav_button">
            <input type="submit" name="Intel_Desktop" value="Intel_Desktop" class="nav_button">
            <input type="submit" name="Intel_Workstation" value="Intel_workstation" class="nav_button">

        </form>
        <?php
            
   
          
        ?>
    </body>
</html>

<?php include('includes/footer.php') ?>